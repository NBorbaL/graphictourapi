﻿using System;
using System.Collections.Generic;
using graphictourapi.Business;
using graphictourapi.Models;
using Microsoft.AspNetCore.Mvc;

namespace graphictourapi.Controllers
{
    [Route("api/[controller]")]
    public class PlaceController : Controller
    {
        [HttpGet]
        [Obsolete]
        public IEnumerable<Place> Get()
        {
            return null;
        }

        [HttpGet("{id}")]
        public Place Get(int id)
        {
            return new PlaceBusiness().GetPlace(id);
        }
    }
}

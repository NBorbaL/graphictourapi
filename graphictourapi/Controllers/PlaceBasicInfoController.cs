﻿using System.Collections.Generic;
using graphictourapi.Business;
using graphictourapi.Models;
using Microsoft.AspNetCore.Mvc;

namespace graphictourapi.Controllers
{
    [Route("api/[controller]")]
    public class PlaceBasicInfoController : Controller
    {
        [HttpGet]
        public IEnumerable<PlaceBasicInfo> Get()
        {
            return new PlaceBasicInfoBusiness().GetAllPlacesBasicInfo();
        }

        [HttpGet("{id}")]
        public PlaceBasicInfo Get(int id)
        {
            return new PlaceBasicInfoBusiness().GetPlaceBasicInfo(id);
        }

        [HttpPost]
        public int Post([FromBody] PlaceBasicInfo placeBasicInfo)
        {
            return new PlaceBasicInfoBusiness().SavePlaceBasicInfo(placeBasicInfo);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using CloudinaryDotNet.Actions;
using graphictourapi.Database;
using graphictourapi.Database.Cdn;
using graphictourapi.Models;

namespace graphictourapi.Business
{
    public class PlaceBasicInfoBusiness
    {
        private PlaceBasicInfoDataAccess placeBasicInfoDataAccess = new PlaceBasicInfoDataAccess(); 

        public PlaceBasicInfoBusiness()
        {
        }

        /// <summary>
        /// Retrieves the basic info for the requested place
        /// </summary>
        /// <param name="id">Id of the place to search</param>
        /// <returns>Basic info of the requested place</returns>
        public PlaceBasicInfo GetPlaceBasicInfo(int id)
        {  
            DataTable placesDataTable = placeBasicInfoDataAccess.GetPlaceBasicInfo(id);
            if (placesDataTable != null && placesDataTable.Rows.Count > 0)
            {
                DataRow placeDataRow = placesDataTable.Rows[0];

                PlaceBasicInfo placeBasicInfo = new PlaceBasicInfo();
                placeBasicInfo.id = int.Parse(placeDataRow[PlaceBasicInfo.ID].ToString());
                placeBasicInfo.name = placeDataRow[PlaceBasicInfo.NAME].ToString();
                placeBasicInfo.description = placeDataRow[PlaceBasicInfo.DESCRIPTION].ToString();

                Image logo = new Image();
                logo.cdnId = placeDataRow[PlaceBasicInfo.LOGO].ToString();
                placeBasicInfo.logo = logo;

                return placeBasicInfo;
            }
            return null;
        }

        /// <summary>
        /// Retrieves the basic info for all the places that are registered
        /// </summary>
        /// <returns>List of all places containing only their basic info</returns>
        public List<PlaceBasicInfo> GetAllPlacesBasicInfo()
        {
            List<PlaceBasicInfo> placeBasicInfoList = new List<PlaceBasicInfo>();
            DataTable placesDataTable = placeBasicInfoDataAccess.GetAllPlacesBasicInfo();
            if (placesDataTable != null)
            {
                foreach (DataRow placeDataRow in placesDataTable.Rows)
                {
                    PlaceBasicInfo placeBasicInfo = new PlaceBasicInfo();
                    placeBasicInfo.id = int.Parse(placeDataRow[PlaceBasicInfo.ID].ToString());
                    placeBasicInfo.name = placeDataRow[PlaceBasicInfo.NAME].ToString();
                    placeBasicInfo.description = placeDataRow[PlaceBasicInfo.DESCRIPTION].ToString();

                    Image logo = new Image();
                    logo.cdnId = placeDataRow[PlaceBasicInfo.LOGO].ToString();
                    placeBasicInfo.logo = logo;

                    // Add place to list
                    placeBasicInfoList.Add(placeBasicInfo);
                }
            }
            return placeBasicInfoList;
        }


        public int SavePlaceBasicInfo(PlaceBasicInfo placeBasicInfo)
        {
            if (placeBasicInfo == null)
            {
                throw new Exception("PlaceBasicInfo is null");
            }

            if (string.IsNullOrWhiteSpace(placeBasicInfo.name))
            {
                throw new Exception("Place name is null");
            }

            if (string.IsNullOrWhiteSpace(placeBasicInfo.description))
            {
                throw new Exception("Place description is null");
            }

            if (placeBasicInfo.logo == null)
            {
                placeBasicInfo.logo = new Image();
            }

            if (placeBasicInfo.id > 0)
            {
                throw new NotImplementedException();
            }
            else
            {
                // Logo upload to CDN
                if (placeBasicInfo.logo.source != null)
                {
                    CloudinaryConnection cloudinaryConnection = new CloudinaryConnection();
                    ImageUploadResult imageUploadResult = cloudinaryConnection.UploadImage(placeBasicInfo.logo.source);
                    if (imageUploadResult != null
                        && (imageUploadResult.Error == null 
                            || string.IsNullOrWhiteSpace(imageUploadResult.Error.Message)))
                    {
                        placeBasicInfo.logo.cdnId = imageUploadResult.PublicId;
                    }
                }
                return placeBasicInfoDataAccess.InsertPlaceBasicInfo(placeBasicInfo);
            }
        }
    }
}

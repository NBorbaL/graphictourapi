﻿using graphictourapi.Models;

namespace graphictourapi.Business
{
    public class PlaceBusiness
    {
        private PlaceBasicInfoBusiness placeBasicInfoBusiness = new PlaceBasicInfoBusiness(); 

        public PlaceBusiness()
        {
        }

        /// <summary>
        /// Retrieves all info for the requested place
        /// </summary>
        /// <param name="id">Id of the place to search</param>
        /// <returns>All info of the requested place</returns>
        public Place GetPlace(int id)
        {
            PlaceBasicInfo placeBasicInfo = placeBasicInfoBusiness.GetPlaceBasicInfo(id);
            // If it is different than null the place exists
            if (placeBasicInfo != null)
            {
                Place place = new Place();
                place.basicInfo = placeBasicInfo;
                return place;
            }
            return null;
        }
    }
}

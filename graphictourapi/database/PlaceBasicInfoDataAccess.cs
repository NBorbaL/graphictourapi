﻿using System.Collections.Generic;
using System.Data;
using graphictourapi.Models;

namespace graphictourapi.Database
{
    public class PlaceBasicInfoDataAccess : BaseDataAccess
    {
        public PlaceBasicInfoDataAccess()
        {
        }

        public DataTable GetPlaceBasicInfo(int id)
        {
            string query = $"SELECT {PlaceBasicInfo.ID}, " +
                $"{PlaceBasicInfo.NAME}, " +
                $"{PlaceBasicInfo.DESCRIPTION}, " +
                $"{PlaceBasicInfo.LOGO} " +
                $"FROM {PlaceBasicInfo.TABLE} P " +
                $"WHERE {PlaceBasicInfo.ID} = @id";

            return ExecuteNonQuery(query, new List<Models.Database.DbParameter>()
            {
                new Models.Database.DbParameter("id", id)
            });
        }

        public DataTable GetAllPlacesBasicInfo()
        {
            string query = $"SELECT {PlaceBasicInfo.ID}, " +
                $"{PlaceBasicInfo.NAME}, " +
                $"{PlaceBasicInfo.DESCRIPTION}, " +
                $"{PlaceBasicInfo.LOGO} " +
                $"FROM {PlaceBasicInfo.TABLE} P";
            return ExecuteNonQuery(query, null);
        }

        public int InsertPlaceBasicInfo(PlaceBasicInfo placeBasicInfo)
        {
            string query = $"INSERT INTO {PlaceBasicInfo.TABLE} ({ PlaceBasicInfo.NAME}, " +
                $"{PlaceBasicInfo.DESCRIPTION}, " +
                $"{PlaceBasicInfo.LOGO}) " +
                $"VALUES (@name, @description, @logo); " +
                $"SELECT SCOPE_IDENTITY();"; // Para retornar o id inserido
            return ExecuteStatement(query, new List<Models.Database.DbParameter>()
            {
                new Models.Database.DbParameter("name", placeBasicInfo.name),
                new Models.Database.DbParameter("description", placeBasicInfo.description),
                new Models.Database.DbParameter("logo", placeBasicInfo.logo.cdnId ?? "")
            });
        }
    }
}

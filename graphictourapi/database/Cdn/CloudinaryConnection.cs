﻿using System;
using System.IO;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace graphictourapi.Database.Cdn
{
    public class CloudinaryConnection
    {
        public const string LOGO_FOLDER_NAME = "GRAPHIC_TOUR_LOGOS";

        private Account account = new Account(
            "dzlxluu4s", // Cloud Name
            "122541273367464", // API Key 
            "xX2-_chylOxJm-G6Sgceh8_vS9c" // API Secret
            );

        private Cloudinary cloudinary; 

        public CloudinaryConnection()
        {
            this.cloudinary = new Cloudinary(account);
        }

        public CloudinaryConnection(Account account)
        {
            this.cloudinary = new Cloudinary(account);
        }

        /// <summary>
        /// Uploads image to CDN Cloudinary
        /// </summary>
        /// <param name="imageBytes">byte[] of image</param>
        /// <param name="folderName">Name of folder in which the image will be uploaded to</param>
        /// <param name="returnSecureUri">true = HTTPS Uri | false = HTTP Uri</param>
        /// <returns>HTTP/HTTPS URI for uploaded image</returns>
        public ImageUploadResult UploadImage(byte[] imageBytes, string folderName = LOGO_FOLDER_NAME, bool returnSecureUri = true)
        {
            string imageName = Guid.NewGuid().ToString();
            MemoryStream imageStream = new MemoryStream(imageBytes);
            ImageUploadParams imageUploadParams = new ImageUploadParams();
            imageUploadParams.File = new FileDescription(imageName, imageStream);
            imageUploadParams.Folder = folderName;
            return cloudinary.Upload(imageUploadParams);
        } 
    }
}

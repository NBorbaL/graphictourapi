﻿using System;
namespace graphictourapi.Models
{
    public class Phone
    {
        public Phone()
        {
        }

        public string countryCode { get; set; }
        public string areaCode { get; set; }
        public string phone { get; set; }
        public string description { get; set; }
    }
}

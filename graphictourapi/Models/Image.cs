﻿using System;
using System.Text;

namespace graphictourapi.Models
{
    public class Image
    {
        public Image()
        {
        }

        public byte[] source { get; set; }
        public string cdnId { get; set; }
        public int order { get; set; }

        public static byte[] stringToByteArray(string str)
        {
            if (str == null)
            {
                return null;
            }
            return Convert.FromBase64String(str);
        }

        public static string byteArrayToString(byte[] byteArray)
        {
            if (byteArray == null)
            {
                return "";
            }
            return Convert.ToBase64String(byteArray);
        }
    }
}

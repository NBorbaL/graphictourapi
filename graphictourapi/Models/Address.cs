﻿namespace graphictourapi.Models
{
    public class Address
    {
        public Address()
        {
        }

        public LatLng coordinates { get; set; }
        public string streetName { get; set; }
        public string streetNumber { get; set; }
        public string lineTwo { get; set; }
        public string zipCode { get; set; }
        public City city { get; set; }
    }
}

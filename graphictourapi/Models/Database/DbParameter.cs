﻿namespace graphictourapi.Models.Database
{
    public class DbParameter
    {
        public DbParameter()
        {
        }

        public DbParameter(string name, object value)
        {
            this.name = name;
            this.value = value;
        }

        public string name { get; set; }
        public object value { get; set; }

    }
}

﻿using System.Collections.Generic;

namespace graphictourapi.Models
{
    public class Place
    {
        public Place()
        {
        }
        
        public PlaceBasicInfo basicInfo { get; set; }
        public List<Image> images { get; set; }
        public List<OpeningHours> openingHours { get; set; }
        public List<Address> address { get; set; }
        public List<Phone> phones { get; set; }
        public List<SocialMedia> socialMediaInfo { get; set; }
    }

    public class PlaceBasicInfo
    {
        public const string TABLE = "PLACES";
        public const string ID = "ID";
        public const string NAME = "NAME";
        public const string DESCRIPTION = "DESCRIPTION";
        public const string LOGO = "LOGO";

        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Image logo { get; set; }
    }
}

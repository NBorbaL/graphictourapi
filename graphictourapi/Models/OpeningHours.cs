﻿using System;
namespace graphictourapi.Models
{
    public class OpeningHours
    {
        public OpeningHours(DayOfWeek dayOfWeek,
            DateTime from,
            DateTime to)
        {
            this.dayOfWeek = dayOfWeek;
            this.from = from;
            this.to = to;
        }

        public DayOfWeek dayOfWeek { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }
}

﻿using System;
namespace graphictourapi.Models
{
    public class City
    {
        public City()
        {
        }

        public int id { get; set; }
        public string name { get; set; }
        public State state { get; set; }
    }
}

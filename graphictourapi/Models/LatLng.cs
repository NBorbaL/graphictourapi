﻿using System;
namespace graphictourapi.Models
{
    public class LatLng
    {
        public LatLng()
        {
        }

        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}

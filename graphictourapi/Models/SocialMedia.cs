﻿using System;
namespace graphictourapi.Models
{
    public class SocialMedia
    {
        public enum SocialMediaType
        {
            FACEBOOK = 0,
            TWITTER = 1,
            INSTAGRAM = 2,
            LINKEDIN = 3,
            EMAIL = 4,
            OTHER = 999
        }

        public SocialMedia()
        {
        }

        public SocialMediaType socialMediaType { get; set; }
        public string contactInfo { get; set; }
    }
}

﻿using System;
namespace graphictourapi.Models
{
    public class State
    {
        public State()
        {
        }

        public int id { get; set; }
        public string name { get; set; }
        public Country country { get; set; }
    }
}
